package com.kaida.kaidaquery.exception;

/**
 * @author zxm
 * @date 2020/9/19 21:26
 */


public class QueryException extends Exception {

    public QueryException(String message) {
        super(message);
    }
}
