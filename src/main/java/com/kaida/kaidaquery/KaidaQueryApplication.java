package com.kaida.kaidaquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaidaQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaidaQueryApplication.class, args);
	}

}
